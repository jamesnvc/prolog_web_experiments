:- module(hw_script, [frame_script_content/1,
                      js_script_handler/1]).

:- use_module(library(http/js_write), [javascript/4]).

js_script_handler(_) :-
    frame_script_content(Script),
    format("Content-Type: text/javascript; charset=utf-8~n~n~w", [Script]).

frame_script_content(Script) :-
    \[Script] = {|javascript(_)||
        const parser = new DOMParser();

        const handleResponse = (target, resp) => {
          const doc = parser.parseFromString(resp, "text/html");
          const id = target.getAttribute("data-frame-id");
          const elt = doc.querySelector(`*[data-frame-id="${id}"]`);
          if (target) { target.replaceWith(elt); }
        };

        const findTargetElement = (start_elt) => {
          let elt = start_elt;
          if (elt.getAttribute("data-target")) {
            const target_id = elt.getAttribute("data-target")
            const target = document.querySelector(`*[data-frame-id="${target_id}"]`);
            if (target) { return target; }
          }
          while (elt && !elt.getAttribute("data-frame-id")) {
              elt = elt.parentNode;
          }
          return elt;
        };

        document.body.addEventListener("submit", (evt) => {
          evt.preventDefault();
          evt.stopImmediatePropagation();
          const form = evt.target;
          const target = findTargetElement(form);
          const data = new FormData(form);
          fetch(form.action,
                {method: 'POST',
                 // explicitly not setting content-type
                 // so browser sets boundary properly
                 credentials: 'same-origin',
                 body: data})
          .then((resp) => Promise.all([resp.text(), Promise.resolve(resp)]))
          .then(([body, resp]) => {
              handleResponse(target, body);
              form.reset();
              const loc = window.location;
              const here = `${loc.pathname}${loc.search}${loc.hash}`;
              // TODO: resp.url isn't supported in Safari...but
              // not sure what else to do
              const loc2 = new URL(resp.url);
              const there = `${loc2.pathname}${loc2.search}${loc2.hash}`;
              if (resp.redirected && here !== there) {
                history.pushState({target: target.getAttribute("data-frame-id")},
                                  '',
                                  there);
              }
          })
          .catch(err => {
              console.error("Error submitting form", err);
          })
        });

        const find_link = (element) => {
        let elt = element;
        while (elt &&
                ( elt.nodeType !== Node.ELEMENT_NODE ||
                !elt.getAttribute("href") )) {
            elt = elt.parentNode;
        }
        return elt;
        };

        document.body.addEventListener('click', e => {
          const any_key = e.metaKey || e.altKey || e.ctrlKey || e.shiftKey;
          const link_elt = find_link(e.target);
          const href = link_elt && link_elt.href;
          if (!link_elt || !href) { return; }
          const url = new URL(href);
          const target_elt = link_elt.getAttribute("target") === "_top" ?
                             document.body : findTargetElement(link_elt);
          if (!any_key &&
              ( link_elt.target === "" ||
                  link_elt.target === "_self" ) &&
              e.button === 0 &&
              window.location.hostname === url.hostname &&
              ( !url.port || window.location.port === url.port )) {
            e.preventDefault();
            const loc = window.location;
            const cur_href = loc.pathname + loc.search + loc.hash;
            const rel_href = url.pathname + url.search + url.hash;
            if (cur_href !== rel_href) {
              history.pushState(
                {target: target_elt.getAttribute("data-frame-id")},
                '',
                rel_href);
            }
            fetch(rel_href,
                  {method: 'GET',
                   credentials: 'same-origin'})
            .then(resp => resp.text())
            .then(resp => handleResponse(target_elt, resp))
            .catch(err => {
              console.error("Error fetching page", err);
            })
          }
        });

        window.onpopstate = (event) => {
            const loc = window.location;
            const cur_href = loc.pathname + loc.search + loc.hash;
            const target = event.state ? event.state.target
                  : document.body.getAttribute("data-root-target");
            const target_elt = document.querySelector(
              `[data-frame-id=${target}]`
            );
            fetch(cur_href, {method: 'GET', credentials: 'same-origin'})
            .then(resp => resp.text())
            .then(resp => handleResponse(target_elt, resp))
            .catch(err => {
              console.error("Error fetching page", err);
            })
        };
    |}.
