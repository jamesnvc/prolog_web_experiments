:- module(hotwire, [main/0, go/1]).

:- use_module(library(apply_macros)).
:- use_module(library(crypto), [crypto_password_hash/2]).
:- use_module(library(debug), [debug/3, debug/1]).
:- use_module(library(filesex), [directory_file_path/3, make_directory_path/1]).
:- use_module(library(http/http_client), [http_read_data/3]).
:- use_module(library(http/http_dispatch), [http_redirect/3,
                                            http_dispatch/1,
                                            http_reply_file/3,
                                            http_handler/3,
                                            http_location_by_id/2
                                           ]).
:- use_module(library(http/http_json), [reply_json/1]).
:- use_module(library(http/http_log), [http_log/2]).
:- use_module(library(http/http_parameters), [http_parameters/2]).
:- use_module(library(http/http_path), []).
:- use_module(library(http/http_multipart_plugin)).
:- use_module(library(http/http_session), [http_session_asserta/1,
                                           http_session_retractall/1,
                                           http_session_data/1]).
:- use_module(library(http/html_write), [html//1,
                                         reply_html_page/2,
                                         html_receive//1]).
:- use_module(library(http/thread_httpd), [http_server/2,
                                           http_stop_server/2]).
:- use_module(library(prolog_server), [prolog_server/2]).
:- use_module(library(uuid), [uuid/1]).
:- use_module(library(settings), [setting/4,
                                  setting/2]).
:- use_module(library(syslog), [openlog/3,
                                syslog/3,
                                closelog/0]).
:- use_module(library(thread_pool), [thread_pool_create/3]).
:- use_module(library(tailwind), [start_watching_dirs/3,
                                  stop_watching_dirs/1,
                                  reset_style/1]).

:- multifile thread_pool:create_pool/1.

thread_pool:create_pool(handler_pool) :-
    thread_pool_create(handler_pool, 20, []).


:- use_module(hw_script, [js_script_handler/1]).

config_file(F) :- getenv('CONFIG_FILE', F), !.
config_file('../config.pl').

:- setting(port, integer, 8080, 'Port to run API server on').
:- setting(use_syslog, boolean, false, 'Send debug output to syslog').
:- setting(remote_toplevel_port, integer, 0, 'If non-zero, port to run TCP toplevel on').
:- setting(server_url, atom, 'http://fuchikoma.local:8899', 'External URL this server is visible from').
:- setting(log_topic, atom, 'PrologServer', 'Syslog topic to log under').

%! main is det.
%  Non-interactive entry point for the server
main :-
    config_file(Conf),
    load_settings(Conf),
    setting(port, Port),
    go(Port),
    set_cleanup_handlers,
    syslog(notice, "Server started", []),
    thread_get_message(_). % wait

set_cleanup_handlers :-
    on_signal(term, _, shutdown),
    on_signal(int, _, shutdown),
    on_signal(usr2, _, reload_config).

shutdown(_) :-
    stop_css_watcher,
    setting(port, Port),
    stop(Port),
    halt.

reload_config(_) :-
    syslog(debug, "Reloading config", []),
    config_file(Conf),
    load_settings(Conf).

%! go(+Port) is det.
%  Interactive entry point to start the server.
go(Port) :-
    debug(server), debug(notify), debug(tailwind),
    setting(use_syslog, UseSyslog),
    setting(log_topic, LogTopic),
    start_css_watcher,
    ( UseSyslog -> openlog(LogTopic, [], syslog) ; true ),
    set_prolog_flag(message_context, [thread, time('%Y-%m-%d %T')]),
    config_file(Conf),
    load_settings(Conf),
    http_server(http_dispatch, [port(Port)]),
    maybe_start_toplevel.

stop(Port) :-
    debug(server, "Stopping...", []),
    http_stop_server(Port, []),
    closelog.

maybe_start_toplevel :-
    setting(remote_toplevel_port, Port),
    Port \= 0, !,
    debug(server, "Starting remote toplevel on port ~w", [Port]),
    prolog_server(Port, [allow(ip(127, 0, 0, 1))]).
maybe_start_toplevel.

% Css

user:file_search_path(css, CssDir) :-
    working_directory(Cwd, Cwd),
    directory_file_path(Cwd, "../resources/css", CssDir).

:- dynamic css_watcher/1.
start_css_watcher :-
    stop_css_watcher,
    working_directory(Cwd, Cwd),
    directory_file_path(Cwd, "../resources/css", CssDir),
    ( exists_directory(CssDir) -> true ; make_directory_path(CssDir) ),
    absolute_file_name(css("styles.css"), CssFile, [access(none)]),
    start_watching_dirs([Cwd], CssFile, Watcher),
    assertz(css_watcher(Watcher)).

stop_css_watcher :-
    css_watcher(Watcher), !,
    stop_watching_dirs(Watcher),
    retractall(css_watcher(Watcher)).
stop_css_watcher.

% Routes

:- multifile http:location/3.
:- dynamic http:location/3.

http:location(api, root(api), []).
http:location(css, root(css), []).
http:location(js, root(js), []).

:- http_handler(root(.), home_page_handler,
                [method(get), id(home_page), spawn(handler_pool)]).
:- http_handler(root(register), register_page_handler,
                [method(get), id(register_page), spawn(handler_pool)]).
:- http_handler(root(info), info_page_handler,
                [method(get), id(info_page), spawn(handler_pool)]).
:- http_handler(root(tasks), tasks_page_handler,
                [method(get), id(tasks_page), spawn(handler_pool)]).

:- http_handler(api(task), add_task_handler, [method(post), id(add_task),
                                             spawn(handler_pool)]).
:- http_handler(api(task/Id), complete_task_handler(Id),
                [method(post), id(complete_task), spawn(handler_pool)]).
:- http_handler(api(task/Id/title), edit_task_handler(Id),
                [method(post), id(edit_task), spawn(handler_pool)]).

:- http_handler(api(login), login_form_handler,
                [method(post), id(login_form), spawn(handler_pool)]).
:- http_handler(api(logout), logout_form_handler,
                [method(post), id(logout_form), spawn(handler_pool)]).
:- http_handler(api(counter/inc), inc_counter_handler,
                [method(post), id(counter_inc_form), spawn(handler_pool)]).
:- http_handler(api(register), register_form_handler,
               [method(post), id(register_form), spawn(handler_pool)]).

:- http_handler(js('hotwire_pl.js'), js_script_handler, [method(get), id(hw_script)]).
:- http_handler(css(File), http_reply_file(css(File), []), [method(get), id(css_file)]).
:- http_handler(css('reset.css'), reset_css_handler, [method(get), id(reset_css)]).

% [TODO] Persistence

:- dynamic user_counter/2.
:- dynamic user_password/2.
:- dynamic user_task/3.
:- dynamic task_completed/2.

%% Handlers

current_user_info(_{name: Name, counter: Counter}) :-
    http_session_data(user(Name)), !,
    ( user_counter(Name, Counter) -> true ; Counter = 0 ).
current_user_info(_{}).

% home
home_page_handler(_Request) :-
    current_user_info(Info),
    reply_html_page(title('Hotwire.pl'), [\home_page(Info)]).

% register
register_page_handler(_) :-
    reply_html_page(title('Hotwire.pl - Register'), [\register_page(_{})]).

register_form_handler(Request) :-
    http_read_data(Request, Data, [form_data(form)]),
    memberchk(name=Name, Data),
    ( \+ user_password(Name, _) ), !,
    memberchk(password=Password, Data),
    crypto_password_hash(Password, Hash),
    assertz(user_password(Name, Hash)),
    http_session_retractall(user(_)),
    http_session_asserta(user(Name)),
    http_redirect(see_other, root(.), Request).
register_form_handler(_) :-
    format("Status: 401~n"),
    reply_html_page(title('Hotwire.pl'),
                    [\register_page(_{flash: "Username already taken"})]).

% login
login_form_handler(Request) :-
    http_read_data(Request, Data, [form_data(form)]),
    memberchk(name=Name, Data), memberchk(password=Password, Data),
    user_password(Name, StoredHash),
    crypto_password_hash(Password, StoredHash), !,
    http_session_retractall(user(_)),
    http_session_asserta(user(Name)),
    ( user_counter(Name, _) -> true ; assertz(user_counter(Name, 0)) ),
    http_redirect(see_other, root(.), Request).
login_form_handler(_) :-
    format("Status: 401~n"),
    reply_html_page(title('Hotwire.pl'),
                    [\home_page(_{flash: "Bad credentials"})]).

logout_form_handler(Request) :-
    http_session_retractall(user(_)),
    http_redirect(see_other, root(info), Request).

% info
info_page_handler(_Request) :-
    reply_html_page(title('Hotwire.pl - Info'), [\info_page]).

% counter
inc_counter_handler(Request) :-
    http_session_data(user(Name)),
    ( user_counter(Name, Counter) -> true ; Counter = 0 ),
    retractall(user_counter(Name, _)),
    NewCounter is Counter + 1,
    assertz(user_counter(Name, NewCounter)) ,
    http_redirect(see_other, root(.), Request).
inc_counter_handler(_) :-
    http_location_by_id(counter_inc_form, Loc),
    throw(http_reply(forbidden(Loc))).

% tasks

tasks_page_handler(_) :-
    http_session_data(user(Name)), !,
    findall(task(Id, Title, LastUpdate, CompCount),
            ( user_task(Name, Id, Title),
              ( aggregate_all(t(max(Update), count),
                            task_completed(Id, Update),
                            t(LastUpdate, CompCount))
              -> true
              ;  ( LastUpdate = never, CompCount = 0 )) ),
            Tasks),
    reply_html_page(title('Hotwire.pl - Tasks'), [\done_this(Tasks)]).
tasks_page_handler(_) :-
    http_location_by_id(home_page, Loc),
    throw(http_reply(see_other(Loc))).

add_task_handler(Request) :-
    http_session_data(user(Name)), !,
    http_read_data(Request, Data, [form_data(form)]),
    uuid(Id),
    memberchk(title=Title, Data),
    assertz(user_task(Name, Id, Title)),
    http_location_by_id(tasks_page, Loc),
    http_redirect(see_other, Loc, Request).
add_task_handler(_) :-
    http_location_by_id(add_task, Loc),
    throw(http_reply(forbidden(Loc))).

complete_task_handler(Id, Request) :-
    http_session_data(user(_)), !,
    get_time(Now),
    assertz(task_completed(Id, Now)),
    http_location_by_id(tasks_page, Loc),
    http_redirect(see_other, Loc, Request).
complete_task_handler(Id, _) :-
    http_location_by_id(complete_task(Id), Loc),
    throw(http_reply(forbidden(Loc))).

edit_task_handler(Id, Request) :-
    http_session_data(user(Name)),
    user_task(Name, Id, _),
    http_read_data(Request, Data, [form_data(form)]),
    memberchk(title=NewTitle, Data), !,
    transaction(( retractall(user_task(Name, Id, _)),
                  assertz(user_task(Name, Id, NewTitle)) )),
    http_location_by_id(tasks_page, Loc),
    http_redirect(see_other, Loc, Request).
edit_task_handler(Id, _) :-
    http_location_by_id(complete_task(Id), Loc),
    throw(http_reply(forbidden(Loc))).

% CSS

reset_css_handler(_) :-
    reset_style(Style),
    format("Content-Type: text/css~n~n~s", [Style]).

% content

user:head(default, Head) -->
    html([Head,
          \html_receive(head),
          link([rel(stylesheet), href(#(reset_css))], []),
          link([rel(stylesheet), href(#(css_file('styles.css')))], []),
          link([rel(stylesheet), href(#(css_file('extras.css')))], [])
         ]).

user:body(default, Body) -->
    html(
        body([div(['data-frame-id'(app), 'data-root-target'(app),
                   class(['mx-4'])],
                  [\app_nav, Body]),
              script(src(#(hw_script)), [])])).

%% Pages

% Home Page

home_page(Info) -->
    { _{name: Name, counter: Counter}  :< Info, ! },
    html(
        div('data-frame-id'(main),
            [\flash(Info),
             \auth_form(user(Name)),
             \counter_form(Counter)])
    ).
home_page(Info) -->
    html(div('data-frame-id'(main), [\flash(Info), \auth_form(none)])).

flash(Info) -->
    { _{flash: Msg} :< Info },
    html(
        div([class(['flex', 'justify-center'])],
            div([class(['text-rose-300'])], Msg))
    ).
flash(_) --> [].

auth_form(user(Name)) -->
    !,
    html(
        div(['data-frame-id'(auth),
             class(['w-full', flex, 'flex-row', 'justify-between'])],
            [h1(class('text-xl'), "Hello ~w"-[Name]),
             form([method(post), action(#(logout_form)), 'data-target'(app)],
                  \button("Logout"))])
    ).
auth_form(_) -->
    html(
        div('data-frame-id'(auth),
            [h1(class('text-xl'), "Log In"),
             div(class([flex, 'justify-center']),
                 form([method(post), action(#(login_form)), 'data-target'(app),
                       class([flex, 'flex-col', 'w-1/2', 'gap-1'])],
                      [\input([type(text), name(name), placeholder(name)]),
                       \input([type(password), name(password), placeholder("●●●●●●")]),
                       \button("Login")])),
             a([href(#(register_page)), class('hover:cursor-pointer')],
               h1(class('text-lg'), "Register"))
            ])
    ).


counter_form(Counter) -->
    html(div('data-frame-id'(counter),
             [span(Counter),
              form([action(#(counter_inc_form)), method(post)],
                   \button("Increment"))])).

register_page(Info) -->
    html(
        div('data-frame-id'(main),
            [\flash(Info),
             div('data-frame-id'(auth),
                 [h1(class('text-xl'), "Register"),
                  div(class([flex, 'justify-center']),
                      form([method(post), action(#(register_form)), 'data-target'(app),
                            class([flex, 'flex-col', 'w-1/2', 'gap-1'])],
                           [\input([type(text), name(name), placeholder(name),
                                    required(true)]),
                            \input([type(password), name(password), placeholder("●●●●●●"),
                                    required(true)]),
                            \button("Login")])),
                  a([href(#(home_page)), class('hover:cursor-pointer')],
                    h1(class('text-lg'), "Login"))
                 ])
            ])
    ).

% Tasks list

done_this(Tasks) -->
    html(
        div('data-frame-id'(main),
            [h2("Done This"),
             \tasks_list(Tasks),
             \create_task
            ])
).

create_task -->
    html(
        form([action(#(add_task)), method(post)],
             [label([],
                    [span("Title"),
                     \input([class(['mx-1']), name(title)])]),
              input([class(['p-1']), type(submit), value("Add")],
                    [])])
    ).

tasks_list(Tasks) -->
    html(
        div([class(['flex', 'flex-col', 'items-center', 'max-w-150']),
            'data-frame-id'(tasks_list)],
            \task_items(Tasks)
           )
    ).

task_items([]) --> [].
task_items([Task|Tasks]) -->
    html([\task(Task), \task_items(Tasks)]).

task(task(Id, Title, Latest, CompCount)) -->
    { Latest == never
      -> LastTime = "never"
      ;  format_time(string(LastTime), "%H:%M %a %d %b %Y", Latest) },
    html(
        div([class([flex, 'flex-row', 'w-100%', 'text-lg', 'justify-between',
                   'items-stretch', 'hover:bg-gray-50']),
             'data-frame-id'("task-~w"-[Id])],
            [div([details(class(['border-2', 'border-transparent', 'whitespace-nowrap',
                                 'overflow-hidden', 'overflow-ellipsis', group,
                                 task]),
                          [summary(
                               span([class(['group-attr-open:hidden', 'p-2px',
                                            'border-1', 'border-transparent'])],
                                    Title)),
                           form([action(#(edit_task(Id))), method(post),
                                class(['inline']), autocomplete(off)],
                                [\input([value(Title), name(title)])])
                          ]),
                  div(class(['text-xs',  'text-gray-500']),
                      [span("Completed ~w times"-[CompCount]),
                       span(class('ml-8'), "Last at ~w"-[LastTime])])]),

             div(class(['ml-8']),
                 [\task_button([class(["bg-purple-600", "text-white"]),
                                method(post), action(#(complete_task(Id)))],
                               "✓")])
            ])
    ).

task_button(Attrs, Body) -->
    { selectchk(class(Cls), Attrs, Attrs0) },
    html(
        form([class('h-100%')|Attrs0],
             [button(class(['pl-4', 'pr-4', 'hover:opacity-70', 'h-100%'|Cls]),
                     Body)])
    ).

% Info Page

info_page -->
    html(
        div(['data-frame-id'(main),
             class(["flex-col", "text-purple-500", 'bg-rose-50'])],
            [
             h2(class(['text-lg',
                       'hover:animate-ping', 'hover:text-rose-100']),
                "Info"),
             p("here's some stuff, some information"),
             a(href(/), "Back to home")
            ])
    ).

% Helpers

input(Attrs) -->
    { selectchk(class(OtherCls), Attrs, Attrs1) -> true
      ; OtherCls = [], Attrs1 = Attrs },
    html(
        input([class(['border-1', 'focus:outline-none', 'focus:ring-2',
                      'p-2px', 'focus:ring-pink-200'|OtherCls])
               |Attrs1], [])
    ).

button(Title) -->
    html(
        button(
            class(['hover:bg-pink-200', 'text-black', 'p-1', 'bg-blue-500',
                   'focus:outline-none', 'focus:ring-pink-300', 'focus:ring-2']),
            Title)).

app_nav -->
    html(
        nav([class([flex, 'flex-row', 'gap-1', 'my-1'])],
            \nav_links([home_page-"Main Page",
                        info_page-"Info",
                        tasks_page-"Tasks"
                       ]))).

nav_links([]) --> [].
nav_links([Link-Title|Links]) -->
    html(a([href(#(Link)), 'data-target'(main),
            class(['hover:bg-blue-50', 'bg-pink-50', 'text-black',
                    'no-underline', 'p-1'])],
           Title)),
    nav_links(Links).
